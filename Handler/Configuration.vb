﻿'Copyright by Chiruclan 2009-2013
'Do not remove this Copyright!

Imports System.IO

Namespace Configuration

    Public Class Configuration

        Private KeyList As SortedDictionary(Of String, String) = New SortedDictionary(Of String, String)
        Private CommentList As SortedDictionary(Of String, String) = New SortedDictionary(Of String, String)

        Public Function ReadConfig(ByVal filename As String) As Boolean
            Try
                KeyList.Clear()

                If File.Exists(filename) Then
                    Using ConfigFile As New FileStream(filename, FileMode.OpenOrCreate, FileAccess.Read)
                        Using ConfigReader As New StreamReader(ConfigFile)
                            Dim Current As String()
                            Dim CurrentLine As String
                            Dim CurrentKey As String
                            Dim CurrentValue As String
                            Dim CurrentComment As String = ""

                            Do
                                CurrentLine = ConfigReader.ReadLine()

                                If Not CurrentLine.StartsWith("#") And Not String.IsNullOrEmpty(CurrentLine) And Not String.IsNullOrWhiteSpace(CurrentLine) Then
                                    Current = CurrentLine.Split(New Char() {"="c}, 2)

                                    CurrentKey = Current(0).Trim()
                                    CurrentValue = Current(1).Trim()

                                    KeyList.Add(CurrentKey, CurrentValue)

                                    If Not CurrentComment = "" Then
                                        CommentList.Add(CurrentKey, CurrentComment)
                                        CurrentComment = ""
                                    End If
                                ElseIf CurrentLine.StartsWith("#") Then
                                    If CurrentComment = "" Then
                                        CurrentComment = CurrentLine.Substring(1).Trim()
                                    Else
                                        CurrentComment &= vbNewLine & CurrentLine.Substring(1).Trim()
                                    End If
                                End If

                                If String.IsNullOrEmpty(CurrentLine) Or String.IsNullOrWhiteSpace(CurrentLine) Then
                                    CurrentLine = "#"
                                End If
                            Loop Until CurrentLine Is Nothing

                            Return True
                        End Using
                    End Using
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function SaveConfig(ByVal filename As String) As Boolean
            Try
                Using ConfigFile As New FileStream(filename, FileMode.Create)
                    Using ConfigWriter As New StreamWriter(ConfigFile)
                        Dim PreviousFirstCharacter As String = ""
                        Dim CurrentFirstCharacter As String = ""

                        For Each Key As String In KeyList.Keys()
                            CurrentFirstCharacter = Key.Substring(0, 1)

                            If Not CurrentFirstCharacter = PreviousFirstCharacter Then
                                If Not String.IsNullOrWhiteSpace(PreviousFirstCharacter) And Not String.IsNullOrEmpty(PreviousFirstCharacter) And Not String.IsNullOrWhiteSpace(CurrentFirstCharacter) And Not String.IsNullOrEmpty(CurrentFirstCharacter) Then
                                    ConfigWriter.WriteLine()
                                End If

                                PreviousFirstCharacter = CurrentFirstCharacter
                            End If

                            If CommentExists(Key) Then
                                For Each Comment As String In GetComment(Key).Split(New Char() {vbNewLine})
                                    ConfigWriter.WriteLine("# " & Comment.Trim(vbNewLine).Trim())
                                Next
                            End If

                            ConfigWriter.WriteLine(Key & " = " & KeyList.Item(Key))
                        Next

                        ConfigWriter.Flush()
                        Return True
                    End Using
                End Using
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ClearConfig() As Boolean
            Try
                KeyList.Clear()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function KeyExists(ByVal Key As String) As Boolean
            Try
                If KeyList.ContainsKey(Key) Then
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function AddKey(ByVal Key As String, ByVal Value As String) As Boolean
            Try
                If Not KeyList.ContainsKey(Key) Then
                    KeyList.Add(Key, Value)
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function RemoveKey(ByVal Key As String) As Boolean
            Try
                If KeyList.ContainsKey(Key) Then
                    KeyList.Remove(Key)
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ListKeys() As List(Of String)
            Try
                Return KeyList.Keys.ToList
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function GetKey(ByVal Key As String) As String
            Try
                If KeyList.ContainsKey(Key) Then
                    Return KeyList.Item(Key)
                End If

                Return Nothing
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function SetKey(ByVal Key As String, ByVal Value As String) As Boolean
            Try
                If KeyList.ContainsKey(Key) Then
                    KeyList.Item(Key) = Value
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function CommentExists(ByVal Key As String) As Boolean
            Try
                If CommentList.ContainsKey(Key) Then
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function AddComment(ByVal Key As String, ByVal Value As String) As Boolean
            Try
                If Not CommentList.ContainsKey(Key) Then
                    CommentList.Add(Key, Value)
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function RemoveComment(ByVal Key As String) As Boolean
            Try
                If CommentList.ContainsKey(Key) Then
                    CommentList.Remove(Key)
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ListComments() As List(Of String)
            Try
                Return CommentList.Keys.ToList
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function GetComment(ByVal Key As String) As String
            Try
                If CommentList.ContainsKey(Key) Then
                    Return CommentList.Item(Key)
                End If

                Return Nothing
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function SetComment(ByVal Key As String, ByVal Value As String) As Boolean
            Try
                If CommentList.ContainsKey(Key) Then
                    CommentList.Item(Key) = Value
                    Return True
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class
End Namespace