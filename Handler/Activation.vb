﻿Imports System.IO
Imports Handler.Configuration

Public Class Activation

    Dim solution As UInteger
    Dim version As String
    Dim Config As New Configuration.Configuration
    Dim ConfigCheck As New Configuration.Check

    Public Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        Dim Generator As System.Random = New Random()
        Return Generator.Next(Min, Max + 1)
    End Function

    Private Sub Activation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConfigCheck.CheckAllKeys()
        Config.ReadConfig("Handler.cfg")

        If File.Exists("version") Then
            Dim sFileReader As StreamReader = File.OpenText("version")
            version = sFileReader.ReadLine()
            sFileReader.Close()
        Else
            MsgBox("Es sind nicht alle benötigten Dateien vorhanden!" & vbNewLine & "Klicke auf OK, um die Anwendung zu beenden.")
            Application.Exit()
            Return
        End If

        My.Settings.Reload()
        If My.Settings.Version = version Then
            Handler.Show()
            Return
        End If

        Dim Generator As System.Random = New Random()

        Dim num1 As Integer = Generator.Next(0, 101)
        Dim num2 As Integer = Generator.Next(0, 101)
        Dim code1 As Integer = Generator.Next(0, 4)

        If code1 = 0 Then
            solution = num1 + num2
            tasklbl.Text = num1 & " + " & num2
        ElseIf code1 = 1 Then
            solution = num1 - num2
            tasklbl.Text = num1 & " - " & num2
        ElseIf code1 = 2 Then
            solution = num1 * num2
            tasklbl.Text = num1 & " x " & num2
        ElseIf code1 = 3 Then
            solution = num1 / num2
            tasklbl.Text = num1 & " / " & num2
        End If
    End Sub

    Private Sub activate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles activate.Click
        If solutiontxt.Text = solution.ToString Then
            My.Settings.Version = version
            My.Settings.Save()
            Handler.Show()
        Else
            MsgBox("Die Lösung war leider falsch! Bitte versuche es noch einmal.")
            solutiontxt.Clear()
            solutiontxt.Focus()
        End If
    End Sub

    Private Sub solutiontxt_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles solutiontxt.KeyDown
        If e.KeyCode = Keys.Enter Then
            activate.PerformClick()
        End If
    End Sub
End Class