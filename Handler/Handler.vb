﻿Imports System.IO
Imports System.Threading.Thread
Imports Handler.Configuration

Public Class Handler

    Dim worldInput As StreamWriter
    Dim Config As New Configuration.Configuration
    Dim ConfigCheck As New Configuration.Check

    Private Sub startBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles startBtn.Click
        If Not Directory.Exists(Config.GetKey("Data.Dir") & "/dbc") Then
            MsgBox("Hast du etwa vergessen die DBC zu installieren?")
            Return
        End If

        If Not Directory.Exists(Config.GetKey("Data.Dir") & "/vmaps") And Config.GetKey("Data.Vmaps.Enabled") = "True" Then
            MsgBox("Hast du etwa vergessen die VMaps zu installieren?" &
                   vbNewLine &
                   vbNewLine & "(solltest du die VMaps deaktiviert haben, kannst du diesen Check in der Handler.cfg abschalten)" &
                   vbNewLine & "(du kannst die Config über das Menü vom TrayIcon neu laden)")
            Return
        End If

        If Not Directory.Exists(Config.GetKey("Data.Dir") & "/mmaps") And Config.GetKey("Data.Mmaps.Enabled") = "True" Then
            MsgBox("Hast du etwa vergessen die MMaps zu installieren?" &
                   vbNewLine &
                   vbNewLine & "(solltest du die MMaps deaktiviert haben, kannst du diesen Check in der Handler.cfg abschalten)" &
                   vbNewLine & "(du kannst die Config über das Menü vom TrayIcon neu laden)")
        End If

        Dim checklist As List(Of String) = New List(Of String)

        checklist.Add(Config.GetKey("Nginx.Binary.Path"))
        checklist.Add(Config.GetKey("PHP.Binary.Path"))
        checklist.Add(Config.GetKey("MySQL.Binary.Path.Server"))
        checklist.Add(Config.GetKey("MySQL.Binary.Path.Admin"))
        checklist.Add(Config.GetKey("AuthServer.Binary.Path"))
        checklist.Add(Config.GetKey("WorldServer.Binary.Path"))
        checklist.Add(Config.GetKey("MySQL.Binary.WorkingDir") & "/" & Config.GetKey("MySQL.Binary.ConfigFile"))
        checklist.Add(Config.GetKey("AuthServer.Binary.WorkingDir") & "/" & Config.GetKey("AuthServer.Config"))
        checklist.Add(Config.GetKey("WorldServer.Binary.WorkingDir") & "/" & Config.GetKey("WorldServer.Config"))
        checklist.Add(Config.GetKey("Nginx.Binary.WorkingDir") & "/" & Config.GetKey("Nginx.Config"))
        checklist.Add(Config.GetKey("PHP.Binary.WorkingDir") & "/" & Config.GetKey("PHP.FastCGI.Config"))

        For Each filename As String In checklist
            If Not File.Exists(filename) Then
                MsgBox("Bitte überprüfe die Pfad zu der folgenden Anwendung:" &
                       vbNewLine &
                       vbNewLine & filename)
                Return
            End If
        Next

        'start nginx.exe
        nginx.StartInfo.FileName = Config.GetKey("Nginx.Binary.Path")
        nginx.StartInfo.Arguments = "-c " & Config.GetKey("Nginx.Config")
        nginx.StartInfo.UseShellExecute = False
        nginx.StartInfo.CreateNoWindow = True
        nginx.StartInfo.WorkingDirectory = Config.GetKey("Nginx.Binary.WorkingDir")
        nginx.Start()

        'start php-cgi.exe
        php_fastcgi.StartInfo.FileName = Config.GetKey("PHP.Binary.Path")
        php_fastcgi.StartInfo.Arguments = "-c " & Config.GetKey("PHP.FastCGI.Config") & " -b " & Config.GetKey("PHP.FastCGI.Hostname") & ":" & Config.GetKey("PHP.FastCGI.Port")
        php_fastcgi.StartInfo.UseShellExecute = False
        php_fastcgi.StartInfo.CreateNoWindow = True
        php_fastcgi.StartInfo.WorkingDirectory = Config.GetKey("PHP.Binary.WorkingDir")
        php_fastcgi.Start()

        'start mysqld.exe
        mysqld.StartInfo.FileName = Config.GetKey("MySQL.Binary.Path.Server")
        mysqld.StartInfo.Arguments = "--defaults-file=" & Config.GetKey("MySQL.Binary.ConfigFile")
        mysqld.StartInfo.UseShellExecute = False
        mysqld.StartInfo.CreateNoWindow = True
        mysqld.StartInfo.WorkingDirectory = Config.GetKey("MySQL.Binary.WorkingDir")
        mysqld.Start()

        Sleep(2000)

        'start authserver
        authserver.StartInfo.FileName = Config.GetKey("AuthServer.Binary.Path")
        authserver.StartInfo.Arguments = "-c " & Config.GetKey("AuthServer.Config")
        authserver.StartInfo.WorkingDirectory = Config.GetKey("AuthServer.Binary.WorkingDir")
        authserver.StartInfo.UseShellExecute = False
        authserver.StartInfo.CreateNoWindow = True
        authserver.StartInfo.RedirectStandardOutput = True
        authserver.StartInfo.RedirectStandardError = True
        authserver.Start()

        Try
            authserver.BeginErrorReadLine()
            authserver.BeginOutputReadLine()
        Catch
        End Try

        'start worldserver
        worldserver.StartInfo.FileName = Config.GetKey("WorldServer.Binary.Path")
        worldserver.StartInfo.Arguments = "-c " & Config.GetKey("WorldServer.Config")
        worldserver.StartInfo.WorkingDirectory = Config.GetKey("WorldServer.Binary.WorkingDir")
        worldserver.StartInfo.UseShellExecute = False
        worldserver.StartInfo.CreateNoWindow = True
        worldserver.StartInfo.RedirectStandardError = True
        worldserver.StartInfo.RedirectStandardOutput = True
        worldserver.StartInfo.RedirectStandardInput = True
        worldserver.Start()

        Try
            worldserver.BeginErrorReadLine()
            worldserver.BeginOutputReadLine()
        Catch
        End Try

        worldInput = worldserver.StandardInput

        'disable start button
        startBtn.Enabled = False

        'enable stop button
        stopBtn.Enabled = True

        'enable worldserverent
        worldserverent.Enabled = True

        'enable status timer
        statusTmr.Enabled = True
    End Sub

    Private Sub stopBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stopBtn.Click
        'disable status timer
        statusTmr.Enabled = False

        'disable worldserverent
        worldserverent.Enabled = False

        'stop stream reading for worldserver
        Try
            worldserver.CancelOutputRead()
            worldserver.CancelErrorRead()
        Catch
        End Try

        'close streamwriter for worldserver
        worldInput.Close()

        'stop worldserver
        If worldserver.HasExited = False Then
            worldserver.Kill()
        End If

        'stop stream reading for authserver
        Try
            authserver.CancelOutputRead()
            authserver.CancelErrorRead()
        Catch
        End Try

        'stop authserver
        If authserver.HasExited = False Then
            authserver.Kill()
        End If

        'stop nginx.exe
        If nginx.HasExited = False Then
            nginx.Kill()
            Shell("taskkill /im " & Config.GetKey("Nginx.Binary.Name") & " /f")
        End If

        'stop mysqld.exe
        If mysqld.HasExited = False Then
            Shell(Config.GetKey("MySQL.Binary.Path.Admin") &
                  " --host=" & Config.GetKey("MySQL.Hostname") &
                  " --port=" & Config.GetKey("MySQL.Port") &
                  " --user=" & Config.GetKey("MySQL.User") &
                  " --password=" & Config.GetKey("MySQL.Password") &
                  " shutdown")
            'mysqld.Kill()
        End If

        'stop php-cgi.exe
        If php_fastcgi.HasExited = False Then
            php_fastcgi.Kill()
        End If

        'disable stop button
        stopBtn.Enabled = False

        'enable start button
        startBtn.Enabled = True

        'setting all states to offline
        loginStatus.ForeColor = Color.DarkRed
        loginStatus.Text = "Offline"
        worldStatus.ForeColor = Color.DarkRed
        worldStatus.Text = "Offline"
        webserverStatus.ForeColor = Color.DarkRed
        webserverStatus.Text = "Offline"
        databaseStatus.ForeColor = Color.DarkRed
        databaseStatus.Text = "Offline"
    End Sub

    Private Sub authserver_OutputDataReceived(ByVal sender As System.Object, ByVal e As System.Diagnostics.DataReceivedEventArgs) Handles authserver.OutputDataReceived
        If authserver.HasExited Then
            Return
        ElseIf String.IsNullOrEmpty(e.Data) Or String.IsNullOrWhiteSpace(e.Data) Then
            Return
        End If
        'append information to textbox
        With authservertb
            Dim fulltext As Integer = .TextLength + vbNewLine.Length + e.Data.Length
            If fulltext >= .MaxLength Then
                .Clear()
            End If
            .Select(.TextLength, 0)
            .SelectionColor = Color.DarkRed
            .AppendText(e.Data.ToString & vbNewLine)
            .SelectionStart = .TextLength
            .ScrollToCaret()
        End With
    End Sub

    Private Sub authserver_ErrorDataReceived(ByVal sender As System.Object, ByVal e As System.Diagnostics.DataReceivedEventArgs) Handles authserver.ErrorDataReceived
        If authserver.HasExited Then
            Return
        ElseIf String.IsNullOrEmpty(e.Data) Or String.IsNullOrWhiteSpace(e.Data) Then
            Return
        End If
        'append error to the textbox
        With authservertb
            Dim fulltext As Integer = .TextLength + vbNewLine.Length + e.Data.Length
            If fulltext >= .MaxLength Then
                .Clear()
            End If
            .Select(.TextLength, 0)
            .SelectionColor = Color.Red
            .AppendText(e.Data.ToString & vbNewLine)
            .SelectionStart = .TextLength
            .ScrollToCaret()
        End With
    End Sub

    Private Sub worldserver_ErrorDataReceived(ByVal sender As System.Object, ByVal e As System.Diagnostics.DataReceivedEventArgs) Handles worldserver.ErrorDataReceived
        If worldserver.HasExited Then
            Return
        ElseIf String.IsNullOrEmpty(e.Data) Or String.IsNullOrWhiteSpace(e.Data) Then
            Return
        End If
        'append error to textbox
        With worldservertb
            Dim fulltext As Integer = .TextLength + vbNewLine.Length + e.Data.Length
            If fulltext >= .MaxLength Then
                .Clear()
            End If
            .Select(.TextLength, 0)
            .SelectionColor = Color.Red
            .AppendText(e.Data.ToString & vbNewLine)
            .SelectionStart = .TextLength
            .ScrollToCaret()
        End With
    End Sub

    Private Sub worldserver_OutputDataReceived(ByVal sender As System.Object, ByVal e As System.Diagnostics.DataReceivedEventArgs) Handles worldserver.OutputDataReceived
        If worldserver.HasExited Then
            Return
        ElseIf String.IsNullOrEmpty(e.Data) Or String.IsNullOrWhiteSpace(e.Data) Then
            Return
        End If
        'append information to textbox
        With worldservertb
            Dim fulltext As Integer = .TextLength + vbNewLine.Length + e.Data.Length
            If fulltext >= .MaxLength Then
                .Clear()
            End If
            .Select(.TextLength, 0)
            .SelectionColor = Color.DarkRed
            .AppendText(e.Data.ToString & vbNewLine)
            .SelectionStart = .TextLength
            .ScrollToCaret()
        End With
    End Sub

    Private Sub toolsBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolsBtn.Click
        'open tools directory
        Shell("EXPLORER tools")
    End Sub

    Private Sub worldserverent_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles worldserverent.KeyDown
        If Not String.IsNullOrEmpty(worldserverent.Text) And Not String.IsNullOrWhiteSpace(worldserverent.Text) And Not worldserver.HasExited Then
            If e.KeyCode = Keys.Enter Then
                worldInput.WriteLine(worldserverent.Text)
                worldInput.Flush()
                worldserverent.Clear()
                worldserverent.Focus()
            End If
        End If
    End Sub

    Private Sub authserver_Exited(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles authserver.Exited
        Try
            authserver.CancelErrorRead()
            authserver.CancelOutputRead()
        Catch
        End Try

        With authservertb
            .Select(.TextLength, 0)
            .SelectionColor = Color.DarkRed
            .AppendText("----------------------" & vbNewLine &
                        "- AuthServer stopped -" & vbNewLine &
                        "----------------------")
        End With
    End Sub

    Private Sub worldserver_Exited(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles worldserver.Exited
        Try
            worldserver.CancelErrorRead()
            worldserver.CancelOutputRead()
        Catch
        End Try

        With worldservertb
            .Select(.TextLength, 0)
            .SelectionColor = Color.DarkRed
            .AppendText("-----------------------" & vbNewLine &
                        "- WorldServer stopped -" & vbNewLine &
                        "-----------------------")
        End With
    End Sub

    Private Sub Handler_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Activation.Close()
        My.Settings.Reload()
        Me.Text = My.Application.Info.Title & " " & My.Settings.Version
    End Sub

    Private Sub Handler_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If stopBtn.Enabled Then
            stopBtn.PerformClick()
        End If
    End Sub

    Private Sub statusTmr_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles statusTmr.Tick

        If Not authserver.HasExited And loginStatus.Text.ToString = "Offline" Then
            loginStatus.ForeColor = Color.Green
            loginStatus.Text = "Online"
        ElseIf authserver.HasExited And loginStatus.Text.ToString = "Online" Then
            loginStatus.ForeColor = Color.DarkRed
            loginStatus.Text = "Offline"
        End If

        If authserver.HasExited And restartCbx.Checked Then
            With authservertb
                .Clear()
                .Select(.TextLength, 0)
                .SelectionColor = Color.DarkRed
                .AppendText("-----------------------------" & vbNewLine &
                            "- AuthServer started -" & vbNewLine &
                            "-----------------------------")
            End With
            authserver.Start()

            Try
                authserver.BeginErrorReadLine()
                authserver.BeginOutputReadLine()
            Catch
            End Try
        End If

        If Not worldserver.HasExited And worldStatus.Text.ToString = "Offline" Then
            worldStatus.ForeColor = Color.Green
            worldStatus.Text = "Online"
        ElseIf worldserver.HasExited And worldStatus.Text.ToString = "Online" Then
            worldStatus.ForeColor = Color.DarkRed
            worldStatus.Text = "Offline"
        End If

        If worldserver.HasExited And restartCbx.Checked Then
            With worldservertb
                .Clear()
                .Select(.TextLength, 0)
                .SelectionColor = Color.DarkRed
                .AppendText("-----------------------" & vbNewLine &
                            "- WorldServer started -" & vbNewLine &
                            "-----------------------")
            End With

            worldserver.Start()

            Try
                worldserver.BeginOutputReadLine()
                worldserver.BeginErrorReadLine()
            Catch
            End Try

            worldInput = worldserver.StandardInput
        End If
        If File.Exists(Config.GetKey("Nginx.PidFile")) Then
            If Not nginx.HasExited And webserverStatus.Text.ToString = "Offline" Then
                webserverStatus.ForeColor = Color.Green
                webserverStatus.Text = "Online"
            ElseIf nginx.HasExited And webserverStatus.Text.ToString = "Online" Then
                webserverStatus.ForeColor = Color.DarkRed
                webserverStatus.Text = "Offline"
            End If
        Else
            If webserverStatus.Text.ToString = "Online" Then
                webserverStatus.ForeColor = Color.DarkRed
                webserverStatus.Text = "Offline"
            End If
        End If

        If File.Exists(Config.GetKey("MySQL.PidFile")) Then
            If Not mysqld.HasExited And databaseStatus.Text.ToString = "Offline" Then
                databaseStatus.ForeColor = Color.Green
                databaseStatus.Text = "Online"
            ElseIf mysqld.HasExited And databaseStatus.Text.ToString = "Online" Then
                databaseStatus.ForeColor = Color.DarkRed
                databaseStatus.Text = "Offline"
            End If
        Else
            If databaseStatus.Text.ToString = "Online" Then
                databaseStatus.ForeColor = Color.DarkRed
                databaseStatus.Text = "Offline"
            End If
        End If
    End Sub

    Private Sub dataBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dataBtn.Click
        Data.Show()
    End Sub

    Private Sub Handler_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            taskbarIcon.ShowBalloonTip(10)
        End If
    End Sub

    Private Sub taskbarIcon_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles taskbarIcon.MouseDoubleClick
        If Me.Visible Then
            Me.Hide()
            taskbarIcon.ShowBalloonTip(10)
        Else
            Me.Show()

            If Me.WindowState = FormWindowState.Minimized Then
                Me.WindowState = FormWindowState.Normal
            End If
        End If
    End Sub

    Private Sub Handler_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Config.ReadConfig("Handler.cfg")
    End Sub

    Private Sub trayIconMenu_reloadConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles trayIconMenu_reloadConfig.Click
        Config.ReadConfig("Handler.cfg")
    End Sub
End Class
