﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Activation
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Activation))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tasklbl = New System.Windows.Forms.Label()
        Me.activate = New System.Windows.Forms.Button()
        Me.solutiontxt = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(598, 76)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Danke, dass du diesen Handler verwendest!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Anscheinend hast du deine Version des " & _
            "Handlers noch nicht aktiviert, deshalb musst du vorher folgende Rechenaufgabe lö" & _
            "sen:"
        '
        'tasklbl
        '
        Me.tasklbl.BackColor = System.Drawing.SystemColors.ControlDark
        Me.tasklbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tasklbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tasklbl.ForeColor = System.Drawing.Color.Lime
        Me.tasklbl.Location = New System.Drawing.Point(12, 85)
        Me.tasklbl.Name = "tasklbl"
        Me.tasklbl.Size = New System.Drawing.Size(598, 64)
        Me.tasklbl.TabIndex = 1
        Me.tasklbl.Text = "0 + 0"
        Me.tasklbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'activate
        '
        Me.activate.BackColor = System.Drawing.SystemColors.ControlDark
        Me.activate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.activate.Location = New System.Drawing.Point(535, 152)
        Me.activate.Name = "activate"
        Me.activate.Size = New System.Drawing.Size(75, 21)
        Me.activate.TabIndex = 2
        Me.activate.Text = "Aktivieren"
        Me.activate.UseVisualStyleBackColor = False
        '
        'solutiontxt
        '
        Me.solutiontxt.BackColor = System.Drawing.SystemColors.ControlDark
        Me.solutiontxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.solutiontxt.Location = New System.Drawing.Point(13, 153)
        Me.solutiontxt.Name = "solutiontxt"
        Me.solutiontxt.Size = New System.Drawing.Size(516, 20)
        Me.solutiontxt.TabIndex = 3
        Me.solutiontxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Activation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(622, 179)
        Me.Controls.Add(Me.solutiontxt)
        Me.Controls.Add(Me.activate)
        Me.Controls.Add(Me.tasklbl)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(638, 218)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(638, 218)
        Me.Name = "Activation"
        Me.Text = "Activation"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tasklbl As System.Windows.Forms.Label
    Friend WithEvents activate As System.Windows.Forms.Button
    Friend WithEvents solutiontxt As System.Windows.Forms.TextBox
End Class
