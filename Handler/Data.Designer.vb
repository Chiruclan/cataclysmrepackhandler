﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Data
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Data))
        Me.searchBtn = New System.Windows.Forms.Button()
        Me.searchTxt = New System.Windows.Forms.TextBox()
        Me.generatorRtb = New System.Windows.Forms.RichTextBox()
        Me.startBtn = New System.Windows.Forms.Button()
        Me.stopBtn = New System.Windows.Forms.Button()
        Me.generator = New System.Diagnostics.Process()
        Me.dbcmapsData = New System.Windows.Forms.CheckBox()
        Me.vmapsData = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'searchBtn
        '
        Me.searchBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.searchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.searchBtn.Location = New System.Drawing.Point(503, 12)
        Me.searchBtn.Name = "searchBtn"
        Me.searchBtn.Size = New System.Drawing.Size(75, 23)
        Me.searchBtn.TabIndex = 3
        Me.searchBtn.Text = "Suchen"
        Me.searchBtn.UseVisualStyleBackColor = False
        '
        'searchTxt
        '
        Me.searchTxt.BackColor = System.Drawing.SystemColors.ControlDark
        Me.searchTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.searchTxt.Location = New System.Drawing.Point(12, 12)
        Me.searchTxt.Name = "searchTxt"
        Me.searchTxt.ReadOnly = True
        Me.searchTxt.Size = New System.Drawing.Size(485, 20)
        Me.searchTxt.TabIndex = 4
        '
        'generatorRtb
        '
        Me.generatorRtb.BackColor = System.Drawing.SystemColors.ControlDark
        Me.generatorRtb.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.generatorRtb.Location = New System.Drawing.Point(12, 72)
        Me.generatorRtb.Name = "generatorRtb"
        Me.generatorRtb.Size = New System.Drawing.Size(566, 359)
        Me.generatorRtb.TabIndex = 5
        Me.generatorRtb.Text = ""
        '
        'startBtn
        '
        Me.startBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.startBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.startBtn.Location = New System.Drawing.Point(12, 437)
        Me.startBtn.Name = "startBtn"
        Me.startBtn.Size = New System.Drawing.Size(280, 23)
        Me.startBtn.TabIndex = 6
        Me.startBtn.Text = "Start"
        Me.startBtn.UseVisualStyleBackColor = False
        '
        'stopBtn
        '
        Me.stopBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.stopBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.stopBtn.Location = New System.Drawing.Point(298, 437)
        Me.stopBtn.Name = "stopBtn"
        Me.stopBtn.Size = New System.Drawing.Size(280, 23)
        Me.stopBtn.TabIndex = 7
        Me.stopBtn.Text = "Stop"
        Me.stopBtn.UseVisualStyleBackColor = False
        '
        'generator
        '
        Me.generator.StartInfo.Domain = ""
        Me.generator.StartInfo.LoadUserProfile = False
        Me.generator.StartInfo.Password = Nothing
        Me.generator.StartInfo.StandardErrorEncoding = Nothing
        Me.generator.StartInfo.StandardOutputEncoding = Nothing
        Me.generator.StartInfo.UserName = ""
        Me.generator.SynchronizingObject = Me
        '
        'dbcmapsData
        '
        Me.dbcmapsData.BackColor = System.Drawing.SystemColors.ControlDark
        Me.dbcmapsData.Checked = True
        Me.dbcmapsData.CheckState = System.Windows.Forms.CheckState.Checked
        Me.dbcmapsData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.dbcmapsData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dbcmapsData.Location = New System.Drawing.Point(12, 42)
        Me.dbcmapsData.Name = "dbcmapsData"
        Me.dbcmapsData.Size = New System.Drawing.Size(283, 24)
        Me.dbcmapsData.TabIndex = 0
        Me.dbcmapsData.Text = "DBC und Maps"
        Me.dbcmapsData.UseVisualStyleBackColor = False
        '
        'vmapsData
        '
        Me.vmapsData.BackColor = System.Drawing.SystemColors.ControlDark
        Me.vmapsData.Checked = True
        Me.vmapsData.CheckState = System.Windows.Forms.CheckState.Checked
        Me.vmapsData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.vmapsData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.vmapsData.Location = New System.Drawing.Point(295, 42)
        Me.vmapsData.Name = "vmapsData"
        Me.vmapsData.Size = New System.Drawing.Size(283, 24)
        Me.vmapsData.TabIndex = 1
        Me.vmapsData.Text = "VMaps"
        Me.vmapsData.UseVisualStyleBackColor = False
        '
        'Data
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(590, 473)
        Me.Controls.Add(Me.stopBtn)
        Me.Controls.Add(Me.startBtn)
        Me.Controls.Add(Me.generatorRtb)
        Me.Controls.Add(Me.searchTxt)
        Me.Controls.Add(Me.searchBtn)
        Me.Controls.Add(Me.vmapsData)
        Me.Controls.Add(Me.dbcmapsData)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(606, 511)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(606, 511)
        Me.Name = "Data"
        Me.Text = "Data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents searchBtn As System.Windows.Forms.Button
    Friend WithEvents searchTxt As System.Windows.Forms.TextBox
    Friend WithEvents generatorRtb As System.Windows.Forms.RichTextBox
    Friend WithEvents startBtn As System.Windows.Forms.Button
    Friend WithEvents stopBtn As System.Windows.Forms.Button
    Friend WithEvents generator As System.Diagnostics.Process
    Friend WithEvents vmapsData As System.Windows.Forms.CheckBox
    Friend WithEvents dbcmapsData As System.Windows.Forms.CheckBox
End Class
