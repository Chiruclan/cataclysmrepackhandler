﻿Imports System.IO
Imports Handler.Configuration

Namespace Configuration
    Public Class Check
        Private Function NeededKeys() As List(Of String)
            Try
                Dim Keys As List(Of String) = New List(Of String)

                Keys.Add("MySQL.Hostname")
                Keys.Add("MySQL.Port")
                Keys.Add("MySQL.User")
                Keys.Add("MySQL.Password")
                Keys.Add("MySQL.Binary.ConfigFile")
                Keys.Add("MySQL.Binary.WorkingDir")
                Keys.Add("MySQL.Binary.Path.Server")
                Keys.Add("MySQL.Binary.Path.Admin")
                Keys.Add("MySQL.PidFile")

                Keys.Add("Nginx.Hostname")
                Keys.Add("Nginx.Port")
                Keys.Add("Nginx.Binary.WorkingDir")
                Keys.Add("Nginx.Binary.Path")
                Keys.Add("Nginx.Binary.Name")
                Keys.Add("Nginx.PidFile")
                Keys.Add("Nginx.Config")

                Keys.Add("PHP.FastCGI.Hostname")
                Keys.Add("PHP.FastCGI.Port")
                Keys.Add("PHP.FastCGI.Config")
                Keys.Add("PHP.Binary.WorkingDir")
                Keys.Add("PHP.Binary.Path")

                Keys.Add("AuthServer.Binary.Path")
                Keys.Add("AuthServer.Binary.WorkingDir")
                Keys.Add("AuthServer.Config")

                Keys.Add("WorldServer.Binary.Path")
                Keys.Add("WorldServer.Binary.WorkingDir")
                Keys.Add("WorldServer.Config")

                Keys.Add("Data.Dir")
                Keys.Add("Data.Vmaps.Enabled")
                Keys.Add("Data.Mmaps.Enabled")

                Keys.Add("Tools.DBC.Binary.Path")
                Keys.Add("Tools.Vmaps.Binary.Path")
                Keys.Add("Tools.Mmaps.Binary.Path")

                Return Keys
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Sub CheckAllKeys()
            Try
                If File.Exists("Handler.cfg") Then
                    Dim Config As New Configuration
                    Dim Invalid As Boolean = False
                    Dim Message As String = "Die Handler.cfg wurde zurückgesetzt!" &
                        vbNewLine & "Es fehlten folgende Einstellungen:" &
                        vbNewLine

                    Config.ReadConfig("Handler.cfg")

                    For Each Key As String In NeededKeys()
                        If Not Config.KeyExists(Key) Then
                            Message &= vbNewLine & Key
                            Invalid = True
                        End If
                    Next

                    If Invalid Then
                        AddDefaults()
                        MsgBox(Message)
                    End If
                Else
                    AddDefaults()
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Sub AddDefaults()
            Try
                Dim Config As New Configuration

                Config.AddKey("MySQL.Hostname", "localhost")
                Config.AddKey("MySQL.Port", "3316")
                Config.AddKey("MySQL.User", "root")
                Config.AddKey("MySQL.Password", "chiruclan")
                Config.AddKey("MySQL.Binary.ConfigFile", "mysql/data/my.ini")
                Config.AddKey("MySQL.PidFile", "udrive/mysql/data/mysql.pid")

                Config.AddKey("MySQL.Binary.WorkingDir", "udrive")
                Config.AddKey("MySQL.Binary.Path.Server", "udrive/mysql/bin/mysqld.exe")
                Config.AddKey("MySQL.Binary.Path.Admin", "udrive/mysql/bin/mysqladmin.exe")

                Config.AddKey("Nginx.Hostname", "localhost")
                Config.AddKey("Nginx.Port", "8096")
                Config.AddKey("Nginx.Binary.WorkingDir", "udrive")
                Config.AddKey("Nginx.Binary.Path", "udrive/nginx.exe")
                Config.AddKey("Nginx.Binary.Name", "nginx.exe")
                Config.AddKey("Nginx.PidFile", "udrive/logs/nginx.pid")
                Config.AddKey("Nginx.Config", "conf/nginx.conf")

                Config.AddKey("PHP.FastCGI.Hostname", "localhost")
                Config.AddKey("PHP.FastCGI.Port", "9000")
                Config.AddKey("PHP.FastCGI.Config", "php/php.ini")
                Config.AddKey("PHP.Binary.WorkingDir", "udrive")
                Config.AddKey("PHP.Binary.Path", "udrive/php/php-cgi.exe")

                Config.AddKey("AuthServer.Binary.Path", "authserver.exe")
                Config.AddKey("AuthServer.Binary.WorkingDir", ".")
                Config.AddKey("AuthServer.Config", "authserver.conf")

                Config.AddKey("WorldServer.Binary.Path", "worldserver.exe")
                Config.AddKey("WorldServer.Binary.WorkingDir", ".")
                Config.AddKey("WorldServer.Config", "worldserver.conf")

                Config.AddKey("Data.Dir", "data")
                Config.AddKey("Data.Vmaps.Enabled", "True")
                Config.AddKey("Data.Mmaps.Enabled", "False")

                Config.AddKey("Tools.DBC.Binary.Path", "tools/DBC&Maps/extractor.exe")
                Config.AddKey("Tools.Vmaps.Binary.Path", "tools/VMaps/vmaps.bat")
                Config.AddKey("Tools.Mmaps.Binary.Path", "tools/MMaps/mmaps_generator.exe")

                Config.SaveConfig("Handler.cfg")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub
    End Class
End Namespace
