﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Handler
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Handler))
        Me.startBtn = New System.Windows.Forms.Button()
        Me.stopBtn = New System.Windows.Forms.Button()
        Me.toolsBtn = New System.Windows.Forms.Button()
        Me.worldservertb = New System.Windows.Forms.RichTextBox()
        Me.worldserverent = New System.Windows.Forms.TextBox()
        Me.authservertb = New System.Windows.Forms.RichTextBox()
        Me.worldserver = New System.Diagnostics.Process()
        Me.authserver = New System.Diagnostics.Process()
        Me.restartCbx = New System.Windows.Forms.CheckBox()
        Me.dataBtn = New System.Windows.Forms.Button()
        Me.statusTmr = New System.Windows.Forms.Timer(Me.components)
        Me.loginStatusLbl = New System.Windows.Forms.Label()
        Me.worldStatusLbl = New System.Windows.Forms.Label()
        Me.webserverStatusLbl = New System.Windows.Forms.Label()
        Me.databaseStatusLbl = New System.Windows.Forms.Label()
        Me.loginStatus = New System.Windows.Forms.Label()
        Me.worldStatus = New System.Windows.Forms.Label()
        Me.webserverStatus = New System.Windows.Forms.Label()
        Me.databaseStatus = New System.Windows.Forms.Label()
        Me.taskbarIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.nginx = New System.Diagnostics.Process()
        Me.php_fastcgi = New System.Diagnostics.Process()
        Me.mysqld = New System.Diagnostics.Process()
        Me.trayIconMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.trayIconMenu_reloadConfig = New System.Windows.Forms.ToolStripMenuItem()
        Me.trayIconMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'startBtn
        '
        Me.startBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.startBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.startBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.startBtn.Location = New System.Drawing.Point(12, 12)
        Me.startBtn.Name = "startBtn"
        Me.startBtn.Size = New System.Drawing.Size(211, 38)
        Me.startBtn.TabIndex = 0
        Me.startBtn.Text = "Start"
        Me.startBtn.UseVisualStyleBackColor = False
        '
        'stopBtn
        '
        Me.stopBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.stopBtn.Enabled = False
        Me.stopBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.stopBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stopBtn.Location = New System.Drawing.Point(12, 56)
        Me.stopBtn.Name = "stopBtn"
        Me.stopBtn.Size = New System.Drawing.Size(211, 38)
        Me.stopBtn.TabIndex = 1
        Me.stopBtn.Text = "Stop"
        Me.stopBtn.UseVisualStyleBackColor = False
        '
        'toolsBtn
        '
        Me.toolsBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.toolsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.toolsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.toolsBtn.Location = New System.Drawing.Point(229, 100)
        Me.toolsBtn.Name = "toolsBtn"
        Me.toolsBtn.Size = New System.Drawing.Size(200, 77)
        Me.toolsBtn.TabIndex = 2
        Me.toolsBtn.Text = "Tools"
        Me.toolsBtn.UseVisualStyleBackColor = False
        '
        'worldservertb
        '
        Me.worldservertb.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.worldservertb.BackColor = System.Drawing.SystemColors.ControlDark
        Me.worldservertb.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.worldservertb.Location = New System.Drawing.Point(435, 12)
        Me.worldservertb.Name = "worldservertb"
        Me.worldservertb.ReadOnly = True
        Me.worldservertb.Size = New System.Drawing.Size(532, 529)
        Me.worldservertb.TabIndex = 3
        Me.worldservertb.Text = ""
        '
        'worldserverent
        '
        Me.worldserverent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.worldserverent.BackColor = System.Drawing.SystemColors.ControlDark
        Me.worldserverent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.worldserverent.Enabled = False
        Me.worldserverent.Location = New System.Drawing.Point(435, 547)
        Me.worldserverent.Name = "worldserverent"
        Me.worldserverent.Size = New System.Drawing.Size(532, 20)
        Me.worldserverent.TabIndex = 4
        '
        'authservertb
        '
        Me.authservertb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.authservertb.BackColor = System.Drawing.SystemColors.ControlDark
        Me.authservertb.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.authservertb.Location = New System.Drawing.Point(12, 183)
        Me.authservertb.Name = "authservertb"
        Me.authservertb.ReadOnly = True
        Me.authservertb.Size = New System.Drawing.Size(417, 383)
        Me.authservertb.TabIndex = 5
        Me.authservertb.Text = ""
        '
        'worldserver
        '
        Me.worldserver.StartInfo.Domain = ""
        Me.worldserver.StartInfo.LoadUserProfile = False
        Me.worldserver.StartInfo.Password = Nothing
        Me.worldserver.StartInfo.StandardErrorEncoding = Nothing
        Me.worldserver.StartInfo.StandardOutputEncoding = Nothing
        Me.worldserver.StartInfo.UserName = ""
        Me.worldserver.SynchronizingObject = Me
        '
        'authserver
        '
        Me.authserver.StartInfo.Domain = ""
        Me.authserver.StartInfo.LoadUserProfile = False
        Me.authserver.StartInfo.Password = Nothing
        Me.authserver.StartInfo.StandardErrorEncoding = Nothing
        Me.authserver.StartInfo.StandardOutputEncoding = Nothing
        Me.authserver.StartInfo.UserName = ""
        Me.authserver.SynchronizingObject = Me
        '
        'restartCbx
        '
        Me.restartCbx.BackColor = System.Drawing.SystemColors.ControlDark
        Me.restartCbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.restartCbx.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.restartCbx.Location = New System.Drawing.Point(229, 71)
        Me.restartCbx.Name = "restartCbx"
        Me.restartCbx.Size = New System.Drawing.Size(200, 23)
        Me.restartCbx.TabIndex = 6
        Me.restartCbx.Text = "automatischer Neustart"
        Me.restartCbx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.restartCbx.UseVisualStyleBackColor = False
        '
        'dataBtn
        '
        Me.dataBtn.BackColor = System.Drawing.SystemColors.ControlDark
        Me.dataBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.dataBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dataBtn.Location = New System.Drawing.Point(229, 12)
        Me.dataBtn.Name = "dataBtn"
        Me.dataBtn.Size = New System.Drawing.Size(200, 53)
        Me.dataBtn.TabIndex = 7
        Me.dataBtn.Text = "Data"
        Me.dataBtn.UseVisualStyleBackColor = False
        '
        'statusTmr
        '
        Me.statusTmr.Interval = 30000
        '
        'loginStatusLbl
        '
        Me.loginStatusLbl.BackColor = System.Drawing.SystemColors.ControlDark
        Me.loginStatusLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loginStatusLbl.Location = New System.Drawing.Point(15, 97)
        Me.loginStatusLbl.Name = "loginStatusLbl"
        Me.loginStatusLbl.Size = New System.Drawing.Size(100, 23)
        Me.loginStatusLbl.TabIndex = 8
        Me.loginStatusLbl.Text = "Authserver:"
        '
        'worldStatusLbl
        '
        Me.worldStatusLbl.BackColor = System.Drawing.SystemColors.ControlDark
        Me.worldStatusLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.worldStatusLbl.Location = New System.Drawing.Point(15, 117)
        Me.worldStatusLbl.Name = "worldStatusLbl"
        Me.worldStatusLbl.Size = New System.Drawing.Size(100, 23)
        Me.worldStatusLbl.TabIndex = 9
        Me.worldStatusLbl.Text = "Worldserver:"
        '
        'webserverStatusLbl
        '
        Me.webserverStatusLbl.BackColor = System.Drawing.SystemColors.ControlDark
        Me.webserverStatusLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.webserverStatusLbl.Location = New System.Drawing.Point(15, 137)
        Me.webserverStatusLbl.Name = "webserverStatusLbl"
        Me.webserverStatusLbl.Size = New System.Drawing.Size(100, 23)
        Me.webserverStatusLbl.TabIndex = 10
        Me.webserverStatusLbl.Text = "Webserver:"
        '
        'databaseStatusLbl
        '
        Me.databaseStatusLbl.BackColor = System.Drawing.SystemColors.ControlDark
        Me.databaseStatusLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.databaseStatusLbl.Location = New System.Drawing.Point(15, 157)
        Me.databaseStatusLbl.Name = "databaseStatusLbl"
        Me.databaseStatusLbl.Size = New System.Drawing.Size(100, 23)
        Me.databaseStatusLbl.TabIndex = 11
        Me.databaseStatusLbl.Text = "Datenbank:"
        '
        'loginStatus
        '
        Me.loginStatus.BackColor = System.Drawing.SystemColors.ControlDark
        Me.loginStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loginStatus.ForeColor = System.Drawing.Color.DarkRed
        Me.loginStatus.Location = New System.Drawing.Point(120, 97)
        Me.loginStatus.Name = "loginStatus"
        Me.loginStatus.Size = New System.Drawing.Size(100, 23)
        Me.loginStatus.TabIndex = 12
        Me.loginStatus.Text = "Offline"
        Me.loginStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'worldStatus
        '
        Me.worldStatus.BackColor = System.Drawing.SystemColors.ControlDark
        Me.worldStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.worldStatus.ForeColor = System.Drawing.Color.DarkRed
        Me.worldStatus.Location = New System.Drawing.Point(120, 117)
        Me.worldStatus.Name = "worldStatus"
        Me.worldStatus.Size = New System.Drawing.Size(100, 23)
        Me.worldStatus.TabIndex = 13
        Me.worldStatus.Text = "Offline"
        Me.worldStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'webserverStatus
        '
        Me.webserverStatus.BackColor = System.Drawing.SystemColors.ControlDark
        Me.webserverStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.webserverStatus.ForeColor = System.Drawing.Color.DarkRed
        Me.webserverStatus.Location = New System.Drawing.Point(120, 137)
        Me.webserverStatus.Name = "webserverStatus"
        Me.webserverStatus.Size = New System.Drawing.Size(100, 23)
        Me.webserverStatus.TabIndex = 14
        Me.webserverStatus.Text = "Offline"
        Me.webserverStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'databaseStatus
        '
        Me.databaseStatus.BackColor = System.Drawing.SystemColors.ControlDark
        Me.databaseStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.databaseStatus.ForeColor = System.Drawing.Color.DarkRed
        Me.databaseStatus.Location = New System.Drawing.Point(120, 157)
        Me.databaseStatus.Name = "databaseStatus"
        Me.databaseStatus.Size = New System.Drawing.Size(100, 23)
        Me.databaseStatus.TabIndex = 15
        Me.databaseStatus.Text = "Offline"
        Me.databaseStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'taskbarIcon
        '
        Me.taskbarIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.taskbarIcon.BalloonTipText = "Klick hier, um das Fenster wieder anzuzeigen."
        Me.taskbarIcon.BalloonTipTitle = "Chiruclan's Repack Handler"
        Me.taskbarIcon.Icon = CType(resources.GetObject("taskbarIcon.Icon"), System.Drawing.Icon)
        Me.taskbarIcon.Text = "Chiruclan's Repack Handler"
        Me.taskbarIcon.Visible = True
        '
        'nginx
        '
        Me.nginx.StartInfo.Domain = ""
        Me.nginx.StartInfo.LoadUserProfile = False
        Me.nginx.StartInfo.Password = Nothing
        Me.nginx.StartInfo.StandardErrorEncoding = Nothing
        Me.nginx.StartInfo.StandardOutputEncoding = Nothing
        Me.nginx.StartInfo.UserName = ""
        Me.nginx.SynchronizingObject = Me
        '
        'php_fastcgi
        '
        Me.php_fastcgi.StartInfo.Domain = ""
        Me.php_fastcgi.StartInfo.LoadUserProfile = False
        Me.php_fastcgi.StartInfo.Password = Nothing
        Me.php_fastcgi.StartInfo.StandardErrorEncoding = Nothing
        Me.php_fastcgi.StartInfo.StandardOutputEncoding = Nothing
        Me.php_fastcgi.StartInfo.UserName = ""
        Me.php_fastcgi.SynchronizingObject = Me
        '
        'mysqld
        '
        Me.mysqld.StartInfo.Domain = ""
        Me.mysqld.StartInfo.LoadUserProfile = False
        Me.mysqld.StartInfo.Password = Nothing
        Me.mysqld.StartInfo.StandardErrorEncoding = Nothing
        Me.mysqld.StartInfo.StandardOutputEncoding = Nothing
        Me.mysqld.StartInfo.UserName = ""
        Me.mysqld.SynchronizingObject = Me
        '
        'trayIconMenu
        '
        Me.trayIconMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.trayIconMenu_reloadConfig})
        Me.trayIconMenu.Name = "trayIconMenu"
        Me.trayIconMenu.Size = New System.Drawing.Size(153, 48)
        '
        'trayIconMenu_reloadConfig
        '
        Me.trayIconMenu_reloadConfig.Name = "trayIconMenu_reloadConfig"
        Me.trayIconMenu_reloadConfig.Size = New System.Drawing.Size(152, 22)
        Me.trayIconMenu_reloadConfig.Text = "Reload config"
        '
        'Handler
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(979, 579)
        Me.Controls.Add(Me.databaseStatus)
        Me.Controls.Add(Me.webserverStatus)
        Me.Controls.Add(Me.worldStatus)
        Me.Controls.Add(Me.loginStatus)
        Me.Controls.Add(Me.databaseStatusLbl)
        Me.Controls.Add(Me.webserverStatusLbl)
        Me.Controls.Add(Me.worldStatusLbl)
        Me.Controls.Add(Me.loginStatusLbl)
        Me.Controls.Add(Me.dataBtn)
        Me.Controls.Add(Me.restartCbx)
        Me.Controls.Add(Me.authservertb)
        Me.Controls.Add(Me.worldserverent)
        Me.Controls.Add(Me.worldservertb)
        Me.Controls.Add(Me.toolsBtn)
        Me.Controls.Add(Me.stopBtn)
        Me.Controls.Add(Me.startBtn)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(995, 617)
        Me.MinimumSize = New System.Drawing.Size(995, 617)
        Me.Name = "Handler"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Chiruclan's Cataclysm Repack Handler"
        Me.trayIconMenu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents startBtn As System.Windows.Forms.Button
    Friend WithEvents stopBtn As System.Windows.Forms.Button
    Friend WithEvents toolsBtn As System.Windows.Forms.Button
    Friend WithEvents worldservertb As System.Windows.Forms.RichTextBox
    Friend WithEvents worldserverent As System.Windows.Forms.TextBox
    Friend WithEvents authservertb As System.Windows.Forms.RichTextBox
    Friend WithEvents worldserver As System.Diagnostics.Process
    Friend WithEvents authserver As System.Diagnostics.Process
    Friend WithEvents dataBtn As System.Windows.Forms.Button
    Friend WithEvents restartCbx As System.Windows.Forms.CheckBox
    Friend WithEvents statusTmr As System.Windows.Forms.Timer
    Friend WithEvents worldStatus As System.Windows.Forms.Label
    Friend WithEvents loginStatus As System.Windows.Forms.Label
    Friend WithEvents databaseStatusLbl As System.Windows.Forms.Label
    Friend WithEvents webserverStatusLbl As System.Windows.Forms.Label
    Friend WithEvents worldStatusLbl As System.Windows.Forms.Label
    Friend WithEvents loginStatusLbl As System.Windows.Forms.Label
    Friend WithEvents databaseStatus As System.Windows.Forms.Label
    Friend WithEvents webserverStatus As System.Windows.Forms.Label
    Friend WithEvents taskbarIcon As System.Windows.Forms.NotifyIcon
    Friend WithEvents nginx As System.Diagnostics.Process
    Friend WithEvents php_fastcgi As System.Diagnostics.Process
    Friend WithEvents mysqld As System.Diagnostics.Process
    Friend WithEvents trayIconMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents trayIconMenu_reloadConfig As System.Windows.Forms.ToolStripMenuItem

End Class
