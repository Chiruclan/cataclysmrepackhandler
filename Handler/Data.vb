﻿Imports System.IO
Imports Handler.Configuration

Public Class Data

    Dim path As String = ""
    Dim vmapsDataBool As Boolean = False
    Dim Config As New Configuration.Configuration

    Private Sub searchBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles searchBtn.Click
        Dim search As New FolderBrowserDialog
        search.ShowDialog()

        path = search.SelectedPath.ToString & "\"

        searchTxt.Text = path.ToString
    End Sub

    Private Sub startBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles startBtn.Click
        If File.Exists(path & "Wow.exe") And Directory.Exists(path & "Data") Then

            If Not File.Exists(Config.GetKey("Tools.DBC.Binary.Path")) Then

            End If

            If dbcmapsData.Checked Then
                If Not File.Exists(Config.GetKey("Tools.DBC.Binary.Path")) Then
                    MsgBox("Der Pfad zum DBC- & Maps-Extractor ist nicht korrekt!")
                End If

                generator.StartInfo.FileName = Config.GetKey("Tools.DBC.Binary.Path")

                If vmapsData.Checked Then
                    If Not File.Exists(Config.GetKey("Tools.Vmaps.Binary.Path")) Then
                        MsgBox("Der Pfad zum VMaps-Generator ist nicht korrekt!")
                    End If

                    vmapsDataBool = True
                End If
            ElseIf vmapsData.Checked Then
                If Not File.Exists(Config.GetKey("Tools.Vmaps.Binary.Path")) Then
                    MsgBox("Der Pfad zum VMaps-Generator ist nicht korrekt!")
                End If

                generator.StartInfo.FileName = Config.GetKey("Tools.Vmaps.Binary.Path")
            End If

            dbcmapsData.Enabled = False
            vmapsData.Enabled = False

            generator.StartInfo.WorkingDirectory = path
            generator.StartInfo.UseShellExecute = False
            generator.StartInfo.CreateNoWindow = True
            generator.StartInfo.RedirectStandardError = True
            generator.StartInfo.RedirectStandardOutput = True
            generator.Start()
            generator.BeginErrorReadLine()
            generator.BeginOutputReadLine()

            startBtn.Enabled = False
            stopBtn.Enabled = True
        Else
            MsgBox("Die Wow.exe und der Data-Ordner sind im angegebenen Pfad nicht auffindbar!" &
                   vbNewLine & "Bitte gib den richtigen Pfad zu deiner WoW-Installation an.")
        End If
    End Sub

    Private Sub Data_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        stopBtn.Enabled = False
        Config.ReadConfig("Handler.cfg")
    End Sub

    Private Sub generator_Exited(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles generator.Exited
        Try
            generator.CancelErrorRead()
            generator.CancelOutputRead()
        Catch
        End Try

        If stopBtn.Enabled Then
            If vmapsDataBool Then
                vmapsDataBool = False
                generator.StartInfo.FileName = Config.GetKey("Tools.Vmaps.Binary.Path")
                generator.Start()

                Try
                    generator.BeginErrorReadLine()
                    generator.BeginOutputReadLine()
                Catch
                End Try
            Else
                Try
                    If dbcmapsData.Checked Then
                        Directory.Move(path & "dbc", Config.GetKey("Data.Dir") & "/dbc")
                        Directory.Move(path & "maps", Config.GetKey("Data.Dir") & "/maps")
                    End If

                    If vmapsData.Checked Then
                        Directory.Move(path & "vmaps", Config.GetKey("Data.Dir") & "/vmaps")
                        Directory.Delete(path & "Buildings", True)
                    End If
                Catch ex As Exception
                    MsgBox("Es ist ein Fehler aufgetreten! " & vbNewLine & ex.Message.ToString)
                End Try
                stopBtn.Enabled = False
                startBtn.Enabled = True
                dbcmapsData.Enabled = True
                vmapsData.Enabled = True
                MsgBox("Alle Daten wurden erfolgreich erstellt!")
            End If
        End If
    End Sub

    Private Sub stopBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stopBtn.Click
        Try
            generator.CancelErrorRead()
            generator.CancelOutputRead()
        Catch
        End Try

        If Not generator.HasExited Then
            generator.Kill()
        End If

        stopBtn.Enabled = False
        startBtn.Enabled = True

        dbcmapsData.Enabled = True
        vmapsData.Enabled = True
    End Sub

    Private Sub generator_OutputDataReceived(ByVal sender As System.Object, ByVal e As System.Diagnostics.DataReceivedEventArgs) Handles generator.OutputDataReceived
        If generator.HasExited Then
            Return
        ElseIf String.IsNullOrEmpty(e.Data) Or String.IsNullOrWhiteSpace(e.Data) Then
            Return
        End If
        'append information to textbox
        With generatorRtb
            Dim fulltext As Integer = .TextLength + vbNewLine.Length + e.Data.Length
            If fulltext >= .MaxLength Then
                .Clear()
            End If
            .Select(.TextLength, 0)
            .SelectionColor = Color.DarkRed
            .AppendText(e.Data.ToString & vbNewLine)
            .SelectionStart = .TextLength
            .ScrollToCaret()
        End With
    End Sub

    Private Sub generator_ErrorDataReceived(ByVal sender As System.Object, ByVal e As System.Diagnostics.DataReceivedEventArgs) Handles generator.ErrorDataReceived
        If generator.HasExited Then
            Return
        ElseIf String.IsNullOrEmpty(e.Data) Or String.IsNullOrWhiteSpace(e.Data) Then
            Return
        End If
        'append information to textbox
        With generatorRtb
            Dim fulltext As Integer = .TextLength + vbNewLine.Length + e.Data.Length
            If fulltext >= .MaxLength Then
                .Clear()
            End If
            .Select(.TextLength, 0)
            .SelectionColor = Color.Red
            .AppendText(e.Data.ToString & vbNewLine)
            .SelectionStart = .TextLength
            .ScrollToCaret()
        End With
    End Sub
End Class